# CoRNAi statistical analysis

This folder contains R scripts to run statistical tests and generate plots for RNAi combinatorial knockdowns.

### Required software

The scripts use core R functionality and several publicly R available packages listed below. Version numbers in brackets correspond to the versions of the packages that were used to develop and debug these scripts:
- **R** (4.0.2)
- **RStudio** (1.3.1073) - optional, testing functions and running the code step-by-step
- **MASS** (7.3-51.6) - robust linear model fit
- **data.table** (1.14.0) - data aggregation and table reformatting
- **plotrix** (3.7-8) - standard error calculation
- **writexl** (1.3.1) - generating Excel files with multiple sheets containing analysis results
- **BBmisc** (1.11) - reformatting intermediate data
- **ggplot2** (3.3.2) - generation box plots overlaid with jitter plots, generation of heatmaps
- **lattice** (0.20-41) - generation of heatmaps

### Running the scripts

1. Change path of the data folder on the top of the script.
2. Lauch the script. You can run the eitire script at once or execue commends one by one to see the intermediate outputs.
3. Output results are saved as *.xlsx* (tables) and *pdf* (plots) files.

### Primary screen analysis

3 scripts for statistical analysis of the primary screen data, provided in this repository 

[**`Primary-screen-analysis.R`**](./Primary-screen-analysis.R)
- Data import
- Calculation of robustZscores for each treatment, taking into replicate (assay) ID
- Calculation of rubustZscores for each treatment
- Summary statistics for each treatment: median and MAD values for robustZscores
- Robust linear model fits for each pair of genes
	- Selecting the subset containing readouts for the negative control, signle gene knockdowns and double knockdown
	- Preforming robust linear model fit
	- Extracting fit summary: best fitted values for regression coefficients, t-values, standard errors
	- Calculation of p-values characterising significance of the fitted regression coefficients from t-values
	- Calculation of confidence intervals for the fitted regression coefficients
- FDR correction of the p-values for regression coefficients extracted from the robust linear model fitting.
- BIC analysis for the appropriateness of the full linear model versus reduced linear model versions for each pair of genes.
- Saving generated tables in the new `*.xlsx` file.

[**`Boxplots_CoRNAi_pairwise.R`**](./Boxplots_CoRNAi_pairwise.R)
- Data import (Cell profiler results qith the quality control flags from HTM Explorer)
- Calculation of rubustZscores for each treatment
- Generaiting a boxplot overlaid with jitter plot for each pair of genes:
	- Selecting the subset containing readouts for the negative control, signle gene knockdowns and double knockdown
	- Plotting 
	- Saving plots as *pdf* files in a newly created subfolder inside a data folder.

[**`Heatmap_CoRNAi.R`**](./Heatmap_CoRNAi.R)
- Calculation of robustZscores for each treatment, taking into replicate (assay) ID
- Calculation of rubustZscores for each treatment
- Summary statistics for each treatment: median and MAD values for robustZscores
- Robust linear model fits for each pair of genes to extract significance of the interaction term.
- FDR correction of the p-values for regression coefficients extracted from the robust linear model fitting.
- Generating heatmaps for robustZscores
- Generating heatmaps for the p-values indicating significance of interaction terms.



### Secondary screen analysis

Raw image data from the validation experiments (secondary screen) and image analysis results used as an input for statistical analysis are available on demand.

[**`Validation-screen-analysis.R`**](./Validation-screen-analysis.R)
- Data import
- Calculation of robustZscores for each treatment, taking into replicate (assay) ID
- Calculation of rubustZscores for each treatment
- Summary statistics for each treatment: median and MAD values for robustZscores
- Robust linear model fits for each pair of genes
	- Selecting the subset containing readouts for the negative control, signle gene knockdowns and double knockdown
	- Preforming robust linear model fit
	- Extracting fit summary: best fitted values for regression coefficients, t-values, standard errors
	- Calculation of p-values characterising significance of the fitted regression coefficients from t-values
	- Calculation of confidence intervals for the fitted regression coefficients
- FDR correction of the p-values for regression coefficients extracted from the robust linear model fitting.
- BIC analysis for the appropriateness of the full linear model versus reduced linear model versions for each pair of genes.
- Saving generated tables in the new `*.xlsx` file.
- Generaiting a boxplot overlaid with jitter plot for each pair of genes
