#script for cleaning secondary (validation) screen data for submission to IDR
#original (input) folder kept without modification
#output folder cleaned at the beginning
#at the end of the script output folder contains images of required channels with correct channel names.
#information about channel names is in the input table file (in the original data the same channel named differently in different assays or replicates).
#files are renamed such that DAPI is always C01, DiI is C02 and DRAQ5 is C03.
##Comment:dealing with very long pathes. Can temporarily create virtual drives masking parts of the long path: https://stackoverflow.com/questions/26151365/long-path-filename-in-windows-makes-write-table-error-out-in-r

rm(list=ls())           ### remove everything from R-memory
library(BBmisc)
library(stringr)
library(writexl)
library(readxl)
#library(data.table)


inputPathLong<-'Z:/exchange_folder/Anthi-paper_MZ/MicroscopeImagesForUpload/ValidationScreen/III'
inputPath<-'U:'
outputPathLong<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/ValidationScreen-cleaned-repIII'
outputPath<-'V:'
#channelInfoPath<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/ValidationScreen-ChannelRenamingInfo-repl_I_II.csv'
treatmentChannelNamingPath<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/ReplicateIII_data_step1-corr2.xlsx'
geneSiRNAsPath<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/Genes_siRNAs.csv'


#delete output folder if already exists

dd<-unlink(outputPathLong,recursive = TRUE)
if (dd!=0){
  errorCondition("can not delete the old output folder")
}

#create empty output folder
dir.create(outputPathLong)


# make virtual drives to shorten path
if(!file.exists(paste0(inputPath,'/'))){
  system(paste('subst',inputPath,inputPathLong))
}
if(!file.exists(paste0(outputPath,'/'))){
  system(paste('subst',outputPath,outputPathLong))
}

#read excel file with information about naming of treatments and channels

## information about treatments and replicates - old naming
treatment.Replicate.Info<-read_excel(treatmentChannelNamingPath,sheet= 'Treatments.Data')

## renaming information for treatments
treatment.Renaming.Info<-read_excel(treatmentChannelNamingPath,sheet= 'Treatments.Data.NoPairs')

##renaming information for channels
channel.Renaming.Info<-read_excel(treatmentChannelNamingPath,sheet= 'ChannelRenamingInfo')

#create new treatment names

treatment.Channel.Naming<-merge(channel.Info, treatment.Naming)

#clean up treatment Channel table, introduce columns to be propagated to the positions table.
treatment.Channel.Naming$FinalTreatmentTag<-treatment.Channel.Naming$Correction_NewTreatmentTag
emptyPositions<-treatment.Channel.Naming$Correction_NewTreatmentTag==''
treatment.Channel.Naming$FinalTreatmentTag[emptyPositions]<-treatment.Channel.Naming$NewTreatmentTag[emptyPositions]

treatment.Channel.Naming[c('GeneSymbol_GeneA','GeneSymbol_GeneB')]<-str_split_fixed(treatment.Channel.Naming$FinalTreatmentTag,'__',2)
treatment.Channel.Naming[c('GeneSymbol_GeneA','Concentration_GeneA')]<-str_split_fixed(treatment.Channel.Naming$GeneSymbol_GeneA,'_',2)
treatment.Channel.Naming[c('GeneSymbol_GeneB','Concentration_GeneB')]<-str_split_fixed(treatment.Channel.Naming$GeneSymbol_GeneB,'_',2)
treatment.Channel.Naming[c('siRNA.Identifier_GeneA','siRNA.Identifier_GeneB')]<-str_split_fixed(treatment.Channel.Naming$NewSiRNATag,'__',2)

#incude siRNA Ensembl IDs and sequences
siRNA.IDs<-read.csv(file=geneSiRNAsPath,sep=",",header = TRUE)
siRNA.IDs$Gene.Symbol[siRNA.IDs$Gene.Symbol=="XWNeg9"]<-'Neg9'

treatment.Channel.Naming$LineIndex<-1:nrow(treatment.Channel.Naming)

treatment.Channel.Naming<-merge(treatment.Channel.Naming,siRNA.IDs,by.x = c('GeneSymbol_GeneB','siRNA.Identifier_GeneB'),by.y = c('Gene.Symbol','siRNA.ID'),all.x = T,all.y = F,sort = F)
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Ensembl.Accession.Number"]<-"Ensembl.Accession.Number_GeneB"
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Sense.siRNA.Sequence"]="Sense.siRNA.Sequence_GeneB"
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Antisense.siRNA.Sequence"]="Antisense.siRNA.Sequence_GeneB"


treatment.Channel.Naming<-merge(treatment.Channel.Naming,siRNA.IDs,by.x = c('GeneSymbol_GeneA','siRNA.Identifier_GeneA'),by.y = c('Gene.Symbol','siRNA.ID'),all.x = T,all.y = F,sort = F)
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Ensembl.Accession.Number"]<-"Ensembl.Accession.Number_GeneA"
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Sense.siRNA.Sequence"]="Sense.siRNA.Sequence_GeneA"
names(treatment.Channel.Naming)[names(treatment.Channel.Naming)=="Antisense.siRNA.Sequence"]="Antisense.siRNA.Sequence_GeneA"


treatment.Channel.Naming<-treatment.Channel.Naming[order(treatment.Channel.Naming$LineIndex),]

columns.To.Propagate<-c("Assay","Replicate","OldTreatmentTag.FileName","NewSiRNATag","FinalTreatmentTag",
                        "GeneSymbol_GeneA","siRNA.Identifier_GeneA",'Concentration_GeneA',"Ensembl.Accession.Number_GeneA","Sense.siRNA.Sequence_GeneA","Antisense.siRNA.Sequence_GeneA",
                        "GeneSymbol_GeneB","siRNA.Identifier_GeneB",'Concentration_GeneB',"Ensembl.Accession.Number_GeneB","Sense.siRNA.Sequence_GeneB","Antisense.siRNA.Sequence_GeneB"
                        )

#copy data for each Plate
n.Replicates.Treatments<-nrow(treatment.Channel.Naming)

get.OldTreatmentTag.Image.Files<-function(image.Folder.Path,image.File.Reg.Exp, group.Index=2){
  #checks if image files in the position folder exixts
  #only files matching specified regular expression are considered
  # if folder does not exist, files not found, or number of files is less than 3 - returbs NA
  #otherwise returns treatment tag from the first fileName
  if(!dir.exists(image.Folder.Path)){
    return (NA)
  }
  image.files=list.files(path=image.Folder.Path,full.names = FALSE,recursive = FALSE,include.dirs = FALSE, no.. = TRUE,pattern = image.File.Reg.Exp)#".*\\.tif$"
  if (length(image.files)<3){
    return (NA)
  }
  
  fileName.Info<-unlist(str_match_all(image.files[1],pattern = image.File.Reg.Exp))
  
  return (fileName.Info[group.Index])
}


copy.Position.Images<-function(new.Replicate.Path,
                               new.Treatment.Tag,
                               new.SiRNA.Tag,
                               old.Position.Path,
                               position.Index,
                               old.FileName.RegExp,
                               old.DAPI.ChAnnel,
                               old.DiI.Channel,
                               old.DRAQ5.Channel,
                               old.channel.Group.Index=3){
  if(is.na(old.Position.Path)){
    return(NA)
  }
  
  if(!dir.exists(old.Position.Path)){
    return(NA)
  }
  
  #define new position path for images
  new.Position.path<-file.path(new.Replicate.Path,
                              new.Treatment.Tag,
                              sprintf('W00001--%s--%s',new.SiRNA.Tag,new.Treatment.Tag),
                              sprintf('P%05d--%s--%s',position.Index,new.SiRNA.Tag,new.Treatment.Tag)
                              )
  
  #create position folder for images
  dir.create(path=new.Position.path,recursive = TRUE)  
  
  #find old image files and extract channels
  old.Image.Files=list.files(path= old.Position.Path,full.names = FALSE,recursive = FALSE,include.dirs = FALSE, no.. = TRUE,pattern = old.FileName.RegExp)
  image.Files.Match<-do.call(rbind.data.frame, str_match_all(old.Image.Files,pattern = old.FileName.RegExp))
  names(image.Files.Match)[1]<-'Old.FileName'
  names(image.Files.Match)[old.channel.Group.Index]<-'Old.Channel.Index'
  image.Files.Match$Old.Channel.Index<-as.integer(image.Files.Match$Old.Channel.Index)
  
  #find match between old and new channel indexes
  image.Files.Match$New.Channel.Index<-NA
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DAPI.ChAnnel]=1
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DiI.Channel]=2
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DRAQ5.Channel]=3

  #create new file names
  image.Files.Match$New.FileName<-sprintf("Validation_LDL--%s--%s--W00001--P%05d--T00000--Z000--C%02d.ome.tif",new.SiRNA.Tag,new.Treatment.Tag,position.Index,image.Files.Match$New.Channel.Index)
  
  #create old and new File path
  image.Files.Match$Old.FilePath<-file.path(old.Position.Path,image.Files.Match$Old.FileName)
  image.Files.Match$New.FilePath<-file.path(new.Position.path,image.Files.Match$New.FileName)
  
  #copy image files
  image.Files.Match$File.Copied<-mapply(file.copy,image.Files.Match$Old.FilePath,image.Files.Match$New.FilePath)

  return(sum(image.Files.Match$File.Copied, na.rm = TRUE))
}

copy.Replicate.Treatment.Images<-function(replicate.Treatment.Info){
  replicate.Treatment.Output<-list()
  
  cat("********************************************",'\n')
  cat("Processing - assay: ",replicate.Treatment.Info$Assay,"; replicate: ", replicate.Treatment.Info$Replicate,"; Treatment",replicate.Treatment.Info$FinalTreatmentTag,'.\n')
  
  #check if replicate folder found
  replicate.Original.Path<-file.path(inputPath,replicate.Treatment.Info$Assay,replicate.Treatment.Info$Replicate)
  if (dir.exists(replicate.Original.Path)){
    cat("Original replicate folder found",'\n')
  }else{
    cat("Error:Original replicate folder not found",'\n')
    errorCondition("Error:Original replicate folder not found")
  }
  
  #create output directory for the replicate
  replicate.Output.Path<-file.path(outputPath,replicate.Treatment.Info$Assay,replicate.Treatment.Info$Replicate,"data_TT_cp")
  if (!dir.exists(replicate.Output.Path)){
    dir.create(path=replicate.Output.Path,recursive = TRUE)  
  }

  
  Position.Folders.Treatment<-data.frame()
  treatment.Folders.Table<-data.frame(replicate.Treatment.Info)
  
  #dealing with the individual treatment information. Loop starts here in the first script
  treatment.Original.Path<-file.path(replicate.Original.Path,replicate.Treatment.Info$treatment.SubPath)
  if (dir.exists(treatment.Original.Path)){
    cat("Original treatment folder found",'\n')
  }else{
    cat("Error:Original treatment folder not found",'\n')
    errorCondition("Error:Original treatment folder not found")
  }
  
  
  
  well.Reg.Exp<-"W00001--([^\\/-]*)--([^\\/-]*)$"
  well.Folders<-list.files(path = treatment.Original.Path, full.names = FALSE, recursive = FALSE,no..=TRUE,pattern = well.Reg.Exp)

  if(!length(well.Folders)==1){
    cat("Error:Well folder not found for the treatment path:",treatment.Original.Path,'\n')
    treatment.Folders.Table$Well.Folder.Exists[1]<-FALSE
    break
  }else{
    treatment.Folders.Table$Well.Folder.Exists[1]<-TRUE
  }

  well.Info<-unlist(str_match_all(well.Folders[1],pattern = well.Reg.Exp))

  
  positition.Folders.Table<-data.frame(Position.Index=1:100,treatment.SubPath=treatment.Folders.Table$treatment.SubPath[1],OldTreatmentTag.Treatment=treatment.Folders.Table$OldTreatmentTag.Treatment[1])
  positition.Folders.Table[columns.To.Propagate]<-treatment.Folders.Table[1,columns.To.Propagate]
  
  positition.Folders.Table$OldPositionFolderName<-sprintf('P%05d--%s--%s',positition.Folders.Table$Position.Index,well.Info[2],well.Info[3])
  positition.Folders.Table$OldPositionFolderPath<-file.path(treatment.Original.Path,well.Folders[1],positition.Folders.Table$OldPositionFolderName)
  positition.Folders.Table$Position.Folder.Exists<-file.exists(positition.Folders.Table$OldPositionFolderPath)
  positition.Folders.Table$Image.Files.Old.Reg.Exp<-paste0("^([^\\/-]*)",#treatment.Folders.Table$treatment.SubPath[iTreatment],
                                                           "--",
                                                           well.Info[2],
                                                           "--",
                                                           well.Info[3],
                                                           sprintf("--W0001--P%03d--T00000--Z000--C([0-9]{2})\\.ome\\.tif",positition.Folders.Table$Position.Index)
                                                           )
  positition.Folders.Table$OldTreatmentTag.FileName<-mapply(get.OldTreatmentTag.Image.Files,positition.Folders.Table$OldPositionFolderPath,positition.Folders.Table$Image.Files.Old.Reg.Exp)
  positition.Folders.Table$Image.Files.Exist<-!is.na(positition.Folders.Table$OldTreatmentTag.FileName)
  
  
  treatment.Folders.Table$Position.Folders.Exist[1]<-sum(positition.Folders.Table$Position.Folder.Exists)
  treatment.Folders.Table$Position.Folders.With.Image.Files[1]<-sum(positition.Folders.Table$Image.Files.Exist)
  
  PositionTreatmentTag.FileName.Unique<-na.omit(unique(positition.Folders.Table$OldTreatmentTag.FileName))
  
  if(!length(PositionTreatmentTag.FileName.Unique)==1){
    cat("Error:Treatment tag for file names is not unique:",treatment.Original.Path,'\n')
    break
  }else{
    treatment.Folders.Table$OldTreatmentTag.FileName[1]<-PositionTreatmentTag.FileName.Unique
  }
  
  #run actual image copying
  positition.Folders.Table$N.Files.Copied<-mapply(copy.Position.Images,
                                                       replicate.Output.Path,
                                                       treatment.Folders.Table$FinalTreatmentTag[1],
                                                       treatment.Folders.Table$NewSiRNATag[1],
                                                       positition.Folders.Table$OldPositionFolderPath,
                                                       positition.Folders.Table$Position.Index,
                                                       positition.Folders.Table$Image.Files.Old.Reg.Exp,
                                                       replicate.Treatment.Info$OldDapiChannelIndex,
                                                       replicate.Treatment.Info$OldDiIChannelIndex,
                                                       replicate.Treatment.Info$OldDraq5ChannelIndex,
                                                       3)

  treatment.Folders.Table$N.Files.Copied[1]<-sum(positition.Folders.Table$N.Files.Copied,na.rm = TRUE)
  treatment.Folders.Table$N.Files.Copied.Div3[1]<-as.numeric(treatment.Folders.Table$N.Files.copied[1])/3.0
  
  Position.Folders.Treatment<-rbind(Position.Folders.Treatment,positition.Folders.Table)

  
  replicate.Treatment.Output[['Treatment.Well.Data']]<-treatment.Folders.Table
  replicate.Treatment.Output[['Positions.Data']]<-Position.Folders.Treatment
  
  return(replicate.Treatment.Output)
}

Positions.Data<-data.frame()
Treatment.Well.Data<-data.frame()

for (iReplicate.Treatment in 1:n.Replicates.Treatments){#
  Replicate.Treatment.Output<-copy.Replicate.Treatment.Images(replicate.Treatment.Info=convertColsToList(treatment.Channel.Naming[iReplicate.Treatment,]))
  
  Treatment.Well.Data<-rbind(Treatment.Well.Data,Replicate.Treatment.Output[['Treatment.Well.Data']])
  Positions.Data<-rbind(Positions.Data,Replicate.Treatment.Output[['Positions.Data']])

}
dataSheets=list()

dataSheets[['Replicate.Treatment.Data.Original']]<-treatment.Channel.Naming
dataSheets[['Treatment.Well.Data.AfterCopying']]<-Treatment.Well.Data
dataSheets[['Positions.Data']]<-Positions.Data

write_xlsx(x=dataSheets,path =file.path(outputPath,'ValidationScreen-I_II_NamesCorrectedManually.xlsx'))



cat("********************************************",'\n')
cat("Processing finished",'\n')
