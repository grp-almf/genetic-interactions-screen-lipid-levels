#script for cleaning secondary (validation) screen data for submission to IDR
#original (input) folder kept without modification
#output folder cleaned at the beginning
#at the end of the script output folder contains images of required channels with correct channel names.
#information about channel names is in the input table file (in the original data the same channel named differently in different assays or replicates).
#files are renamed such that DAPI is always C01, DiI is C02 and DRAQ5 is C03.
##Comment:dealing with very long pathes. Can temporarily create virtual drives masking parts of the long path: https://stackoverflow.com/questions/26151365/long-path-filename-in-windows-makes-write-table-error-out-in-r

rm(list=ls())           ### remove everything from R-memory
library(BBmisc)
library(stringr)
library(writexl)
#library(data.table)


#inputPath<-'Z:/exchange_folder/Anthi-paper_MZ/MicroscopeImagesForUpload/ValidationScreen'
inputPath<-'U:/'
#outputPath<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/ValidationScreen-cleaned'
outputPath<-'V:/'
channelInfoPath<-'Z:/exchange_folder/Anthi-paper_AH/MicroscopeImagesForUpload/ValidationScreen-ChannelRenamingInfo-repl_I_II.csv'


#delete output folder if already exists

dd<-unlink(outputPath,recursive = TRUE)
if (dd!=0){
  errorCondition("can not delete the old output folder")
}

#create empty output folder
dir.create(outputPath)

#read channel information
channel.Info<-read.csv(file=channelInfoPath,sep=",",header = TRUE)

#copy data for each Plate
n.Replicates<-nrow(channel.Info)

get.OldTreatmentTag.Image.Files<-function(image.Folder.Path,image.File.Reg.Exp, group.Index=2){
  #checks if image files in the position folder exixts
  #only files matching specified regular expression are considered
  # if folder does not exist, files not found, or number of files is less than 3 - returbs NA
  #otherwise returns treatment tag from the first fileName
  if(!dir.exists(image.Folder.Path)){
    return (NA)
  }
  image.files=list.files(path=image.Folder.Path,full.names = FALSE,recursive = FALSE,include.dirs = FALSE, no.. = TRUE,pattern = image.File.Reg.Exp)#".*\\.tif$"
  if (length(image.files)<3){
    return (NA)
  }
  
  fileName.Info<-unlist(str_match_all(image.files[1],pattern = image.File.Reg.Exp))
  
  return (fileName.Info[group.Index])
}

get.New.Treatment.Tag<-function(old.Treatment.Tag){
  tag.Split<-unlist(strsplit(old.Treatment.Tag,',|_'))

  if(length(tag.Split)==4){
    return(paste0(tag.Split[1],'_',tag.Split[2],'__',tag.Split[3],'_',tag.Split[4]))
  }

  if(length(tag.Split)==3){
    return(paste0(tag.Split[1],'_',tag.Split[3],'__',tag.Split[2],'_',tag.Split[3]))
  }

  if(length(tag.Split)==2){
    return(paste0(tag.Split[1],'_',tag.Split[2],'__',tag.Split[1],'_',tag.Split[2]))
  }
  
  return(NA)
  
}

get.New.SiRNA.Tag<-function(old.SiRNA.Tag){
  tag.Split<-unlist(strsplit(old.SiRNA.Tag,'_'))
  if(length(tag.Split)==2){
    return(paste0(tag.Split[1],'__',tag.Split[2]))
  }
  if(length(tag.Split)==1){
    return(paste0(old.SiRNA.Tag,'__',old.SiRNA.Tag))
  }
  
  return(NA)
}

copy.Position.Images<-function(new.Replicate.Path,
                               new.Treatment.Tag,
                               new.SiRNA.Tag,
                               old.Position.Path,
                               position.Index,
                               old.FileName.RegExp,
                               old.DAPI.ChAnnel,
                               old.DiI.Channel,
                               old.DRAQ5.Channel,
                               old.channel.Group.Index=3){
  if(is.na(old.Position.Path)){
    return(NA)
  }
  
  if(!dir.exists(old.Position.Path)){
    return(NA)
  }
  
  #define new position path for images
  new.Position.path<-file.path(new.Replicate.Path,
                              new.Treatment.Tag,
                              sprintf('W00001--%s--%s',new.SiRNA.Tag,new.Treatment.Tag),
                              sprintf('P%05d--%s--%s',position.Index,new.SiRNA.Tag,new.Treatment.Tag)
                              )
  
  #create position folder for images
  dir.create(path=new.Position.path,recursive = TRUE)  
  
  #find old image files and extract channels
  old.Image.Files=list.files(path= old.Position.Path,full.names = FALSE,recursive = FALSE,include.dirs = FALSE, no.. = TRUE,pattern = old.FileName.RegExp)
  image.Files.Match<-do.call(rbind.data.frame, str_match_all(old.Image.Files,pattern = old.FileName.RegExp))
  names(image.Files.Match)[1]<-'Old.FileName'
  names(image.Files.Match)[old.channel.Group.Index]<-'Old.Channel.Index'
  image.Files.Match$Old.Channel.Index<-as.integer(image.Files.Match$Old.Channel.Index)
  
  #find match between old and new channel indexes
  image.Files.Match$New.Channel.Index<-NA
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DAPI.ChAnnel]=1
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DiI.Channel]=2
  image.Files.Match$New.Channel.Index[image.Files.Match$Old.Channel.Index==old.DRAQ5.Channel]=3

  #create new file names
  image.Files.Match$New.FileName<-sprintf("Validation_LDL--%s--%s--W00001--P%05d--T00000--Z000--C%02d.ome.tif",new.SiRNA.Tag,new.Treatment.Tag,position.Index,image.Files.Match$New.Channel.Index)
  
  #create old and new File path
  image.Files.Match$Old.FilePath<-file.path(old.Position.Path,image.Files.Match$Old.FileName)
  image.Files.Match$New.FilePath<-file.path(new.Position.path,image.Files.Match$New.FileName)
  
  #copy image files
  image.Files.Match$File.Copied<-mapply(file.copy,image.Files.Match$Old.FilePath,image.Files.Match$New.FilePath)

  return(sum(image.Files.Match$File.Copied, na.rm = TRUE))
}

copy.Replicate.Images<-function(replicate.Info){
  replicate.Output<-list()
  
  cat("********************************************",'\n')
  cat("Processing assay: ",replicate.Info$Assay,"; replicate: ", replicate.Info$Replicate,'.\n')
  
  #check if replicate folder found
  replicate.Original.Path<-file.path(inputPath,replicate.Info$Assay,replicate.Info$Replicate)
  if (dir.exists(replicate.Original.Path)){
    cat("Original replicate folder found",'\n')
  }else{
    cat("Error:Original replicate folder not found",'\n')
    errorCondition("Error:Original replicate folder not found")
  }
  
  #create output directory for the replicate
  replicate.Output.Path<-file.path(outputPath,replicate.Info$Assay,replicate.Info$Replicate,"data_TT_cp")
  dir.create(path=replicate.Output.Path,recursive = TRUE)  
  
  #list well and position folders
  #well.Position.RegExp<-"^[^-\\/]*--[^-\\/]*--[^-\\/]*[\\/][^-\\/]*--[^-\\/]*--[^-\\/]*$"#
  #well.Position.Folders<-list.dirs(path = plate.Original.Path, full.names = FALSE, recursive = TRUE)#,pattern = well.Position.RegExp
  #well.Position.Folders<-well.Position.Folders[str_detect(well.Position.Folders,well.Position.RegExp)]
  #well.Position.Folders=data.table(Well.Position.SubPath=well.Position.Folders)
  #well.Position.Folders<-well.Position.Folders[, c('Well.SubPath','Position.SubPath') := tstrsplit(Well.Position.SubPath, "/")]
  
  #well.Position.Folders$Plate<-plate.Info$PlateID
  #plate.Output[['n.Well.Positions']]<-nrow(well.Position.Folders)
  
  #list treatment folders
  treatment.RegExp<-"([^\\/]*)_([0-9]{3})$"#
  treatment.Folders<-list.files(path = replicate.Original.Path, full.names = FALSE, recursive = FALSE,no..=TRUE,pattern = treatment.RegExp)
  #treatment.Folders<-treatment.Folders[str_detect(treatment.Folders,treatment.RegExp)]
  
  treatment.Folders.Table<-do.call(rbind.data.frame, str_match_all(treatment.Folders,pattern = treatment.RegExp))
  names(treatment.Folders.Table)<-c('treatment.SubPath','OldTreatmentTag.Treatment','Old.Well.Index')
  treatment.Folders.Table$Well.Folder.Exists<-NA
  treatment.Folders.Table$Position.Folders.Exist<-NA
  treatment.Folders.Table$Position.Folders.With.Image.Files<-NA
  treatment.Folders.Table$OldTreatmentTag.FileName<-NA
  treatment.Folders.Table$N.Files.Copied<-NA
  
  replicate.Output[['n.Treatments']]<-nrow(treatment.Folders.Table)
  
  
  Position.Folders.Treatment<-data.frame()
  for (iTreatment in 1:nrow(treatment.Folders.Table)){
    treatment.Original.Path<-file.path(replicate.Original.Path,treatment.Folders.Table$treatment.SubPath[iTreatment])
    well.Reg.Exp<-"W00001--([^\\/-]*)--([^\\/-]*)$"
    well.Folders<-list.files(path = treatment.Original.Path, full.names = FALSE, recursive = FALSE,no..=TRUE,pattern = well.Reg.Exp)
    #treatment.Folders<-treatment.Folders[str_detect(treatment.Folders,treatment.RegExp)]
    if(!length(well.Folders)==1){
      cat("Error:Well folder not found for the treatment path:",treatment.Original.Path,'\n')
      treatment.Folders.Table$Well.Folder.Exists[iTreatment]<-FALSE
      break
    }else{
      treatment.Folders.Table$Well.Folder.Exists[iTreatment]<-TRUE
    }

    well.Info<-unlist(str_match_all(well.Folders[1],pattern = well.Reg.Exp))
    treatment.Folders.Table$OldTreatmentTag.Well[iTreatment]<-well.Info[3]
    treatment.Folders.Table$OldSiRNATag.Well[iTreatment]<-well.Info[2]

    #swap when saved in wrong order
    if(startsWith(treatment.Folders.Table$OldTreatmentTag.Well[iTreatment],'s')){
      tmp<-treatment.Folders.Table$OldTreatmentTag.Well[iTreatment]
      treatment.Folders.Table$OldTreatmentTag.Well[iTreatment]=treatment.Folders.Table$OldSiRNATag.Well[iTreatment]
      treatment.Folders.Table$OldSiRNATag.Well[iTreatment]=tmp
      
    }
    
    
    positition.Folders.Table<-data.frame(Position.Index=1:100,treatment.SubPath=treatment.Folders.Table$treatment.SubPath[iTreatment],OldTreatmentTag.Treatment=treatment.Folders.Table$OldTreatmentTag.Treatment[iTreatment])
    positition.Folders.Table$OldPositionFolderName<-sprintf('P%05d--%s--%s',positition.Folders.Table$Position.Index,well.Info[2],well.Info[3])
    positition.Folders.Table$OldPositionFolderPath<-file.path(treatment.Original.Path,well.Folders[1],positition.Folders.Table$OldPositionFolderName)
    positition.Folders.Table$Position.Folder.Exists<-file.exists(positition.Folders.Table$OldPositionFolderPath)
    positition.Folders.Table$Image.Files.Old.Reg.Exp<-paste0("^([^\\/-]*)",#treatment.Folders.Table$treatment.SubPath[iTreatment],
                                                             "--",
                                                             well.Info[2],
                                                             "--",
                                                             well.Info[3],
                                                             sprintf("--W0001--P%03d--T00000--Z000--C([0-9]{2})\\.ome\\.tif",positition.Folders.Table$Position.Index)
                                                             )
    positition.Folders.Table$OldTreatmentTag.FileName<-mapply(get.OldTreatmentTag.Image.Files,positition.Folders.Table$OldPositionFolderPath,positition.Folders.Table$Image.Files.Old.Reg.Exp)
    positition.Folders.Table$Image.Files.Exist<-!is.na(positition.Folders.Table$OldTreatmentTag.FileName)
    
    
    treatment.Folders.Table$Position.Folders.Exist[iTreatment]<-sum(positition.Folders.Table$Position.Folder.Exists)
    treatment.Folders.Table$Position.Folders.With.Image.Files[iTreatment]<-sum(positition.Folders.Table$Image.Files.Exist)
    
    PositionTreatmentTag.FileName.Unique<-na.omit(unique(positition.Folders.Table$OldTreatmentTag.FileName))
    
    if(!length(PositionTreatmentTag.FileName.Unique)==1){
      cat("Error:Treatment tag for file names is not unique:",treatment.Original.Path,'\n')
      break
    }else{
      treatment.Folders.Table$OldTreatmentTag.FileName[iTreatment]<-PositionTreatmentTag.FileName.Unique
    }
    
    treatment.Folders.Table$NewTreatmentTag[iTreatment]<-get.New.Treatment.Tag(treatment.Folders.Table$OldTreatmentTag.Treatment[iTreatment])
    treatment.Folders.Table$NewSiRNATag[iTreatment]<-get.New.SiRNA.Tag(treatment.Folders.Table$OldSiRNATag.Well[iTreatment])
    
    #run actual image copying
    positition.Folders.Table$N.Files.Copied<-mapply(copy.Position.Images,
                                                         replicate.Output.Path,
                                                         treatment.Folders.Table$NewTreatmentTag[iTreatment],
                                                         treatment.Folders.Table$NewSiRNATag[iTreatment],
                                                         positition.Folders.Table$OldPositionFolderPath,
                                                         positition.Folders.Table$Position.Index,
                                                         positition.Folders.Table$Image.Files.Old.Reg.Exp,
                                                         replicate.Info$OldDapiChannelIndex,
                                                         replicate.Info$OldDiIChannelIndex,
                                                         replicate.Info$OldDraq5ChannelIndex,
                                                         3)
    
    treatment.Folders.Table$N.Files.Copied[iTreatment]<-sum(positition.Folders.Table$N.Files.Copied,na.rm = TRUE)
    treatment.Folders.Table$N.Files.Copied.Div3[iTreatment]<-as.numeric(treatment.Folders.Table$N.Files.copied[iTreatment])/3.0
    
    Position.Folders.Treatment<-rbind(Position.Folders.Treatment,positition.Folders.Table)
  }
  
  
  
  replicate.Output[['Treatment.Well.Data']]<-treatment.Folders.Table
  replicate.Output[['Positions.Data']]<-Position.Folders.Treatment
  
  return(replicate.Output)
}

#Missing.Well.Data<-data.frame()
#Empty.Position.SubPath<-data.frame()
channel.Info$N.Treatments<-NA
#channel.Info$N.Well.Positions<-NA
#channel.Info$N.Copied.Files<-NA


Positions.Data<-data.frame()
Treatment.Well.Data<-data.frame()

for (iReplicate in 1:n.Replicates){#
  Replicate.Output<-copy.Replicate.Images(replicate.Info=convertColsToList(channel.Info[iReplicate,]))
  
  
  channel.Info$N.Treatments[iReplicate]<-Replicate.Output[['n.Treatments']]
  #channel.Info$N.Well.Positions[iPlate]<-Plate.Output[['n.Well.Positions']]
  #channel.Info$N.Copied.Files[iPlate]<-Plate.Output[['n.Copied.Files']]
  
  Treatment.Well.Data<-rbind(Treatment.Well.Data,Replicate.Output[['Treatment.Well.Data']])
  Positions.Data<-rbind(Positions.Data,Replicate.Output[['Positions.Data']])

}
dataSheets=list()

dataSheets[['Replicate.Data']]<-channel.Info
dataSheets[['Treatment.Well.Data']]<-Treatment.Well.Data
dataSheets[['Positions.Data']]<-Positions.Data

write_xlsx(x=dataSheets,path =file.path(outputPath,'ValidationScreen-I_II.xlsx'))



cat("********************************************",'\n')
cat("Processing finished",'\n')
