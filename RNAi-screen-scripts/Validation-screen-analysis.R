library(MASS)
library(data.table)
library(ggplot2)
library(BBmisc)
library(gridExtra)

rm(list=ls())           ### remove everything from R-memory

data.Path<-'C:/tempdat/Magda-stat/Validation/Validation-Data-Clean/'
data.Summary.FileName<-'Validation-Summary-Complete-Corrected.csv'

output.Path<-file.path(data.Path,'../Validation-R-Output')
output.Path.Plots<-file.path(output.Path,'Plots')


if(!dir.exists(output.Path)){
  dir.create(output.Path)
}

if(!dir.exists(output.Path.Plots)){
  dir.create(output.Path.Plots)
}


#read summary file
data.Summary<-read.csv(file=file.path(data.Path,data.Summary.FileName),sep=",",header = TRUE)


#function that calculates interaction amplitude and p.Value for particular pair
evaluate.Gene.Interaction<-function(gene.Pair.Data){
  pair.Output<-list()
  
  treatment.Neg.Coupling<-data.frame(Metadata_gene=c(gene.Pair.Data$Treatment.Control,gene.Pair.Data$Treatment.Gene1,gene.Pair.Data$Treatment.Gene2,gene.Pair.Data$Treatment.Combi)
                                    ,treatment.Neg=c(gene.Pair.Data$Treatment.Control,gene.Pair.Data$Neg.Gene1,gene.Pair.Data$Neg.Gene2,gene.Pair.Data$Neg.Combi))
  
  assay=gene.Pair.Data$Assay

  cat("Processing pair: ",gene.Pair.Data$Gene1,"--",gene.Pair.Data$Gene2,'\n')
  
  #find the input data files for this particular pair
  data.Files<-list.files(path=file.path(data.Path,assay),pattern = "*.csv",recursive = TRUE,include.dirs = FALSE)
  
  #import and filter experiment tables with Z-scores(HTM explorer output)
  TRUETbl<-data.frame()

  for (data.File in data.Files){ 
    
    Tbl<-read.csv(file.path(data.Path,assay,data.File)) ### read file and save to vector Tbl
    trueTbl<-subset(Tbl,(Tbl$HTM_qc==TRUE)&(Tbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean!="NA")) 
    trueTbl<-subset(trueTbl,select=c("Metadata_gene","Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean"))
    trueTbl$Metadata_gene<-as.factor(trueTbl$Metadata_gene)
    trueTbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean<-as.numeric(trueTbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean)
    
    
    #get normalisation values for calculating robust z scores
    
    medianTbl<-aggregate(trueTbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean,by=list(trueTbl$Metadata_gene),FUN="median",na.rm=TRUE)
    names(medianTbl)<-c("treatment.Neg","medianDiIintensity.Neg")

    MADtbl<-aggregate(trueTbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean,by=list(trueTbl$Metadata_gene),FUN="mad",na.rm=TRUE)
    names(MADtbl)<-c("treatment.Neg","MADofDiIintensity.Neg")
    
    median.MAD.Tbl<-merge(medianTbl,MADtbl,by=c("treatment.Neg"))
    median.MAD.Tbl<-merge(medianTbl,MADtbl,by=c("treatment.Neg"))
    median.MAD.Tbl<-merge(median.MAD.Tbl,treatment.Neg.Coupling,by=c("treatment.Neg"),all.y = TRUE)
    #median.MAD.Tbl<-median.MAD.Tbl[median.MAD.Tbl$treatment %in% c(gene.Pair.Data$Treatment.Control,gene.Pair.Data$Neg.Gene1,gene.Pair.Data$Neg.Gene2,gene.Pair.Data$Neg.Combi),]
    
    gene.Pair.Tbl<-merge(trueTbl,median.MAD.Tbl,by=c("Metadata_gene"),all.y = TRUE)


    gene.Pair.Tbl$robustZscore=(gene.Pair.Tbl$Mean_Cytoplasm_Math_DiITotalIntensityDotsbyMean-gene.Pair.Tbl$medianDiIintensity.Neg)/gene.Pair.Tbl$MADofDiIintensity.Neg
    
    TRUETbl<-rbind(TRUETbl,gene.Pair.Tbl)

  }
  ######## Compute median robust Z score ############
  
  
  robustZscoreTbl<-aggregate(TRUETbl$robustZscore,by=list(TRUETbl$Metadata_gene),FUN="median",na.rm=TRUE)
  madRobZscoreTbl<-aggregate(TRUETbl$robustZscore,by=list(TRUETbl$Metadata_gene),FUN="mad",na.rm=TRUE)
  names(robustZscoreTbl)<-c("treatment","medianRobZscore")
  names(madRobZscoreTbl)<-c("treatment","madRobZscore")
  robustZscoreTbl<-merge(robustZscoreTbl,madRobZscoreTbl,by="treatment")
  row.names(robustZscoreTbl)<-robustZscoreTbl$treatment
  # 
  #################Robust Linear Model Fitting

    
  
  combi<-(TRUETbl$Metadata_gene==gene.Pair.Data$Treatment.Combi)
  single1<-((TRUETbl$Metadata_gene==gene.Pair.Data$Treatment.Gene1)|combi)
  single2<-((TRUETbl$Metadata_gene==gene.Pair.Data$Treatment.Gene2)|combi)
  rlmfit<-rlm(robustZscore~single1+single2+combi,data=TRUETbl) 
  
  #rs <- summary(rlmfit)$sigma 
  rlmTbl<-as.data.frame(coef(summary(rlmfit)))

  names(rlmTbl)[2]<-"Std.Error"
  
  names(rlmTbl)[3]<-"tvalue"
  rlmTbl$pvalue<-(2-2*pnorm(abs(rlmTbl$tvalue)))
  
  #confidence intervals
  rlmTbl$CI=paste(round(rlmTbl$Value-1.96*rlmTbl$Std.Error, digits = 3),round(rlmTbl$Value+1.96*rlmTbl$Std.Error, digits = 3),sep = ",")

  #################Counting data points
  dataCounts=data.frame(
    Count.All=nrow(TRUETbl),
    Count.Double=sum(combi),
    Count.Gene1=sum(single1)-sum(combi),
    Count.Gene2=sum(single2)-sum(combi),
    Count.Control=nrow(TRUETbl)-sum(single1)-sum(single2)+sum(combi)
  )
  
  ####### Generating Plots
  plots.Filename<-paste0(gene.Pair.Data$Gene1,"--",gene.Pair.Data$Gene2,".pdf")
  pdf(file=file.path(output.Path.Plots,plots.Filename))

  TRUETbl$Metadata_gene = factor(TRUETbl$Metadata_gene,
                                 levels=c(gene.Pair.Data$Treatment.Control,gene.Pair.Data$Treatment.Gene1,gene.Pair.Data$Treatment.Gene2,gene.Pair.Data$Treatment.Combi))
  pos = position_jitter(w = .1, h = .0)
  p<- ggplot(TRUETbl, aes(x = Metadata_gene, 
                       y = robustZscore, 
                       group=Metadata_gene, 
                       #colour=Metadata_gene, 
                       ymax= max(robustZscore))) + 
    geom_point(position = pos, alpha=0.6, size=1) + 
    coord_cartesian(ylim=c(gene.Pair.Data$Boxplot.RobustZscore.Min, gene.Pair.Data$Boxplot.RobustZscore.Max)) + 
    geom_boxplot(alpha=0,outlier.colour=NA)+ #alpha sets the transparency, so that datapoints behind box can be seen                        
    theme(axis.title.x = element_blank()) +   # Remove x-axis label
    ylab("median robust Z score") +  
    theme_bw() + # Set y-axis label
    theme(axis.text.x  = element_text(angle=90, vjust=0.5, size=10), panel.border = element_rect(colour="grey"), axis.line = element_line(colour = "grey"))
  
  print(p) 
  dev.off()
  
  ### getRobustZScore individual values for Double treatment
  robustZscore.Datapoints.Double<-TRUETbl[combi,c("Metadata_gene","robustZscore")]
  robustZscore.Datapoints.Double$Treatment<-gene.Pair.Data$Treatment
  
  
  pair.Output[['rlmTbl']]<-rlmTbl
  pair.Output[['robustZScoreTbl']]<-robustZscoreTbl
  pair.Output[['dataCounts']]<-dataCounts
  pair.Output[['robustZscore.Datapoints.Double']]<-robustZscore.Datapoints.Double
  
  
  
  return(pair.Output)
}

data.Summary$Gene1.Effect.Value<-NA
data.Summary$Gene2.Effect.Value<-NA
data.Summary$Combi.Effect.Value<-NA
data.Summary$Gene1.P.Value<-NA
data.Summary$Gene2.P.Value<-NA
data.Summary$Combi.P.Value<-NA
data.Summary$Gene1.CI<-NA
data.Summary$Gene2.CI<-NA
data.Summary$Combi.CI<-NA
data.Summary$Gene1.StdError<-NA
data.Summary$Gene2.StdError<-NA
data.Summary$Combi.StdError<-NA



data.Summary$Gene1.RobustZScore.Median<-NA
data.Summary$Gene2.RobustZScore.Median<-NA
data.Summary$Combi.RobustZScore.Median<-NA
data.Summary$Gene1.RobustZScore.MAD<-NA
data.Summary$Gene2.RobustZScore.MAD<-NA
data.Summary$Combi.RobustZScore.MAD<-NA

data.Summary$Count.All<-NA
data.Summary$Count.Control<-NA
data.Summary$Count.Gene1<-NA
data.Summary$Count.Gene2<-NA
data.Summary$Count.Double<-NA


dataPoints.Double.Merge<-data.frame()

#apply combi analysis for each tested pair
n.Validation.Pairs=nrow(data.Summary)


for (iPair in 1:n.Validation.Pairs){
                        pair.Results<-evaluate.Gene.Interaction(gene.Pair.Data=convertColsToList(data.Summary[iPair,]))
                      
                        rlm.Tbl<-pair.Results[['rlmTbl']]
                        
                        data.Summary$Gene1.Effect.Value[iPair]<-rlm.Tbl['single1TRUE','Value']
                        data.Summary$Gene2.Effect.Value[iPair]<-rlm.Tbl['single2TRUE','Value']
                        data.Summary$Combi.Effect.Value[iPair]<-rlm.Tbl['combiTRUE','Value']
                        data.Summary$Gene1.P.Value[iPair]<-rlm.Tbl['single1TRUE','pvalue']
                        data.Summary$Gene2.P.Value[iPair]<-rlm.Tbl['single2TRUE','pvalue']
                        data.Summary$Combi.P.Value[iPair]<-rlm.Tbl['combiTRUE','pvalue']
                        data.Summary$Gene1.CI[iPair]<-rlm.Tbl['single1TRUE','CI']
                        data.Summary$Gene2.CI[iPair]<-rlm.Tbl['single2TRUE','CI']
                        data.Summary$Combi.CI[iPair]<-rlm.Tbl['combiTRUE','CI']
                        data.Summary$Gene1.StdError[iPair]<-rlm.Tbl['single1TRUE','Std.Error']
                        data.Summary$Gene2.StdError[iPair]<-rlm.Tbl['single2TRUE','Std.Error']
                        data.Summary$Combi.StdError[iPair]<-rlm.Tbl['combiTRUE','Std.Error']
                        
                        summary.ZScore.Tbl<-pair.Results[['robustZScoreTbl']]
                        
                        data.Summary$Gene1.RobustZScore.Median[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Gene1'],"medianRobZscore"]
                        data.Summary$Gene2.RobustZScore.Median[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Gene2'],"medianRobZscore"]
                        data.Summary$Combi.RobustZScore.Median[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Combi'],"medianRobZscore"]
                        
                        data.Summary$Gene1.RobustZScore.MAD[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Gene1'],"madRobZscore"]
                        data.Summary$Gene2.RobustZScore.MAD[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Gene2'],"madRobZscore"]
                        data.Summary$Combi.RobustZScore.MAD[iPair]<-summary.ZScore.Tbl[data.Summary[iPair,'Treatment.Combi'],"madRobZscore"]
                        
                        data.Counts<-pair.Results[['dataCounts']]
                        data.Summary$Count.All[iPair]<-data.Counts$Count.All[1]
                        data.Summary$Count.Control[iPair]<-data.Counts$Count.Control[1]
                        data.Summary$Count.Gene1[iPair]<-data.Counts$Count.Gene1[1]
                        data.Summary$Count.Gene2[iPair]<-data.Counts$Count.Gene2[1]
                        data.Summary$Count.Double[iPair]<-data.Counts$Count.Double[1]
                        
                        
                        
                        
                        dataPoints.Double.Merge<-rbind(dataPoints.Double.Merge, pair.Results[['robustZscore.Datapoints.Double']])

}


# get p-values in the long format for applying multiple comparisons corrections
data.P.Value.Correction<-melt(data=data.Summary[,c('Gene1','Gene2','Gene1.P.Value','Gene2.P.Value','Combi.P.Value')],
                                  id.vars = c('Gene1','Gene2'),
                                  measure.vars = c('Gene1.P.Value','Gene2.P.Value','Combi.P.Value'),
                                  variable.name = 'Model.Term',
                                  value.name = 'p.value')

#applying FDR correction
data.Fdr<-within(data.P.Value.Correction,{
    p.value_fdr<-as.numeric(p.adjust(p.value,method="fdr"))
    Model.Term.Fdr<-paste0(Model.Term,".Fdr")
  })
data.Fdr$Model.Term.Fdr<-factor(data.Fdr$Model.Term.Fdr,levels=unique(data.Fdr$Model.Term.Fdr))
data.Fdr.Wide<-dcast(data=data.Fdr,Gene1+Gene2~Model.Term.Fdr,value.var = "p.value_fdr")
data.Summary<-merge(data.Summary,data.Fdr.Wide,by=c('Gene1','Gene2'))


#applying Bonferroni correction
data.Bonferroni<-within(data.P.Value.Correction,{
  p.value_bonf<-as.numeric(p.adjust(p.value,method="bonferroni"))
  Model.Term.Bonferroni<-paste0(Model.Term,".Bonferroni")
})
data.Bonferroni$Model.Term.Bonferroni<-factor(data.Bonferroni$Model.Term.Bonferroni,levels=unique(data.Bonferroni$Model.Term.Bonferroni))

data.Bonf.Wide<-dcast(data=data.Bonferroni,Gene1+Gene2~Model.Term.Bonferroni,value.var = "p.value_bonf")
data.Summary<-merge(data.Summary,data.Bonf.Wide,by=c('Gene1','Gene2'))


write.csv(x = data.Summary,file = file.path(output.Path,"Calculated-P-Values.csv"),row.names = FALSE)

###Plot of the interaction values and robust-z-scores sorted by the type of interaction
plot.Pairs.Interactions.FileName<-'Interaction-summary-plots--pairs.csv'

plot.Pairs.Interactions<-read.csv(file=file.path(data.Path,plot.Pairs.Interactions.FileName),sep=",",header = TRUE)
plot.Pairs.Interactions$Treatment=factor(plot.Pairs.Interactions$Treatment,levels = plot.Pairs.Interactions$Treatment)
plot.Pairs.Interactions$Label=factor(plot.Pairs.Interactions$Label,levels = plot.Pairs.Interactions$Label)


data.Summary$Treatment<-toupper(data.Summary$Treatment)


data.Interaction.Summary.Plots<-merge(plot.Pairs.Interactions,data.Summary,by=c('Treatment'),all.x = TRUE,all.y = FALSE)

#names(dataPoints.Double.Merge)[1]<-"Treatment"
dataPoints.Double.Merge$Treatment<-toupper(dataPoints.Double.Merge$Treatment)


data.Zscores.Summary.Plots<-merge(plot.Pairs.Interactions,dataPoints.Double.Merge,by=c('Treatment'),all.x = TRUE,all.y = FALSE)

data.Zscores.Summary.Plots<-data.Zscores.Summary.Plots[!is.na(data.Zscores.Summary.Plots$robustZscore),]

##make Zscores plot
#pdf(file=file.path(output.Path,"ZScores-summmary-boxplot.pdf"))

pos = position_jitter(w = .1, h = .0)
p<- ggplot(data.Zscores.Summary.Plots, aes(x = Label, 
                        y = robustZscore, 
                        group=Treatment, 
                        #colour=Metadata_gene, 
                        ymax= max(robustZscore))) +
  scale_x_discrete(limits = rev(levels(data.Zscores.Summary.Plots$Label)))+
  geom_point(position = pos,shape=16, alpha=0.6, size=1) + 
  coord_cartesian(ylim=c(min(data.Zscores.Summary.Plots$robustZscore), max(data.Zscores.Summary.Plots$robustZscore))) +
  #coord_cartesian(ylim=c(-25, 25)) +
  geom_boxplot(alpha=0,outlier.colour=NA)+ #alpha sets the transparency, so that datapoints behind box can be seen                        
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("median robust Z score") +  
  theme_bw() + # Set y-axis label
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),axis.text.x  = element_text(angle=0, vjust=0.5, size=10), panel.border = element_rect(colour="grey"), axis.line = element_line(colour = "grey"))+
  coord_flip()

print(p) 

p.Interaction<-ggplot(data.Interaction.Summary.Plots,
                      aes(x = Label, y = Combi.Effect.Value))+
                      geom_bar(stat="identity", color="gray", position=position_dodge())+
                      geom_errorbar(aes(ymin=Combi.Effect.Value-Combi.StdError, ymax=Combi.Effect.Value+Combi.StdError), width=.4,position=position_dodge(.9))+
                      scale_x_discrete(limits = rev(levels(data.Interaction.Summary.Plots$Label)))+
                      theme_bw() + # Set y-axis label
                      theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(),axis.text.x  = element_text(angle=0, vjust=0.5, size=10), panel.border = element_rect(colour="grey"), axis.line = element_line(colour = "grey"))+
                      coord_flip()
                      #scale_x_discrete(labels=data.Interaction.Summary.Plots$Label) 

print(p.Interaction) 


#dev.off()


gridPlot<-grid.arrange(cbind(ggplotGrob(p), ggplotGrob(p.Interaction+theme(axis.title.y = element_blank(),axis.ticks.y = element_blank(), axis.text.y = element_blank())), size = "last"))

##save gridPlot as PDF
ggsave(filename = file.path(output.Path,"ZScores-interactions-summmary.pdf"),plot=gridPlot)


