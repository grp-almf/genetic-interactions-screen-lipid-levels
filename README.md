# Genetic Interactions Screen on Lipid Levels

## Project Overview

This repository contains protocols for the data analysis for analysing the gene interaction data for LDL uptake screen.

## Repository content
- Image analysis results of RNAi screen (Image analysis performed with [CellProfiler](https://cellprofiler.org/), data quality control performed with [HTMExplorer](https://github.com/tischi/HTM_Explorer)).
- Statistical analysis of RNAi screen data. Refer to this [document](./RNAi-screen-scripts/README.md) for the information about implemented functions and guidelies for using these scripts.
- Statistical analysis of genetic data.  Refer to this [document](./UKBB_analysis/README.md) for the information about implemented functions.

## Contributors
- **Magdalena Zimon** (*Pepperkok Lab, EMBL*): RNAi experiments and analysis, current project maintainer.
- **Yunfeng Huang** (*Biogen*): statistical analysis of genetic data.
- **Anthi Trasta** (*Pepperkok Lab, EMBL*): primary RNAi screen and statistical analysis.
- **Aliaksandr Halavatyi** (*ALMF, EMBL*): statistical data analysis and support.

## Citation

These work is part of the manuscript:

> Genetic interactions modulate lipid plasma levels and cellular uptake. Magdalena Zimon, Yunfeng Huang, Anthi Trasta, Jimmy Liu, Chia-Yen Chen, Aliaksandr Halavatyi, Peter Blattmann, Bernd Klaus, Chris Whelan, David Sexton, Sally John, Ellen Tsai, Wolfgang Huber, Rainer Pepperkok, Heiko Runz

## Acknowledgement
We thank Bernd Klaus (CSDA, EMBL) for the essential advises on statistical analysis

