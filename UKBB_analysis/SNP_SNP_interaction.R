#######################################################
######### UKBB lipids SNP-SNP interaction #############
######### Author: Yunfeng Huang           #############
#######################################################

# Read phenotype data
library(data.table)
lipids_pheno <- fread("/camhpc/home/yhuang5/lipids_heiko/SNP_interaction/2021/ukb_cauc_unrel_lipids.pheno.tsv.gz")

# Generating residuals for HDL, LDL, total cholesterol and triglycerides adjusted for age, sex, smoking status (current vs. past vs. never),
# alcohol drinking (current vs. past vs. never), BMI, cholesterol medication use (self-reported) and top 10 genotype PCs

row.names(lipids_pheno) <- lipids_pheno$eid
hdl <- lm(HDL~age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=lipids_pheno)
hdl_res <- as.data.frame(hdl$residuals)
hdl_res$eid <- as.numeric(row.names(hdl_res))
colnames(hdl_res)[1] <- "hdl_res"
lipids_pheno <- merge(lipids_pheno,hdl_res,by="eid",all.x=T)

row.names(lipids_pheno) <- lipids_pheno$eid
ldl <- lm(LDL~age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=lipids_pheno)
ldl_res <- as.data.frame(ldl$residuals)
ldl_res$eid <- as.numeric(row.names(ldl_res))
colnames(ldl_res)[1] <- "ldl_res"
lipids_pheno <- merge(lipids_pheno,ldl_res,by="eid",all.x=T)

row.names(lipids_pheno) <- lipids_pheno$eid
tc <- lm(TC~age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=lipids_pheno)
tc_res <- as.data.frame(tc$residuals)
tc_res$eid <- as.numeric(row.names(tc_res))
colnames(tc_res)[1] <- "tc_res"
lipids_pheno <- merge(lipids_pheno,tc_res,by="eid",all.x=T)

row.names(lipids_pheno) <- lipids_pheno$eid
tg <- lm(TG~age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=lipids_pheno)
tg_res <- as.data.frame(tg$residuals)
tg_res$eid <- as.numeric(row.names(tg_res))
colnames(tg_res)[1] <- "tg_res"
lipids_pheno <- merge(lipids_pheno,tg_res,by="eid",all.x=T)

# Test for SNP-SNP interaction through robust linear model fitting and extract BIC as well as interaction model coefficients (beta and t-value) 
library(MASS)
lipids_pheno <- as.data.frame(lipids_pheno)

# HDL
snps <- colnames(lipids_pheno)[12:39]

hdl_results <- as.data.frame(matrix(NA,378,12))
n <- 0
for(i in 1:27){
  for(j in (i+1):28){
    n <- n+1
    sub <- lipids_pheno[,c(snps[i],snps[j],"hdl_res")]
    colnames(sub) <- c("snp1","snp2","hdl_res")
    model1 <- rlm(hdl_res~snp1,data=sub)
    model2 <- rlm(hdl_res~snp2,data=sub)
    model3 <- rlm(hdl_res~snp1+snp2,data=sub)
    model4 <- rlm(hdl_res~snp1+snp2+snp1:snp2,data=sub)
    hdl_results[n,1] <- snps[i]
    hdl_results[n,2] <- snps[j]
    hdl_results[n,3] <- BIC(model1)
    hdl_results[n,4] <- BIC(model2)
    hdl_results[n,5] <- BIC(model3)
    hdl_results[n,6] <- BIC(model4)
    hdl_results[n,7] <- summary(model4)$coeff[2,1]
    hdl_results[n,8] <- summary(model4)$coeff[3,1]
    hdl_results[n,9] <- summary(model4)$coeff[4,1]
    hdl_results[n,10] <- summary(model4)$coeff[2,3]
    hdl_results[n,11] <- summary(model4)$coeff[3,3]
    hdl_results[n,12] <- summary(model4)$coeff[4,3]
  }
  print(i)
}
rm(i,j,n,sub,model1,model2,model3,model4)
colnames(hdl_results) <- c("SNP1","SNP2","model1_BIC","model2_BIC","model3_BIC","model4_BIC","SNP1_E","SNP2_E","SNP1SNP2_E","SNP1_t","SNP2_t","SNP1SNP2_t")

hdl_results$SNP1_P <- pnorm(abs(hdl_results$SNP1_t),lower.tail = F)*2
hdl_results$SNP2_P <- pnorm(abs(hdl_results$SNP2_t),lower.tail = F)*2
hdl_results$SNP1SNP2_P <- pnorm(abs(hdl_results$SNP1SNP2_t),lower.tail = F)*2

hdl_results_long <- as.data.frame(matrix(NA,378*3,4))
colnames(hdl_results_long) <- c("SNP1","SNP2","name","P")

n <- 1
for(i in 1:378){
  hdl_results_long[n:(n+2),1] <- hdl_results[i,1]
  hdl_results_long[n:(n+2),2] <- hdl_results[i,2]
  hdl_results_long[n:(n+2),3] <- c("SNP1","SNP2","SNP1SNP2")
  hdl_results_long[n:(n+2),4] <- as.numeric(hdl_results[i,13:15])
  n <- n+3
}

# LDL
ldl_results <- as.data.frame(matrix(NA,378,12))
n <- 0
for(i in 1:27){
  for(j in (i+1):28){
    n <- n+1
    sub <- lipids_pheno[,c(snps[i],snps[j],"ldl_res")]
    colnames(sub) <- c("snp1","snp2","ldl_res")
    model1 <- rlm(ldl_res~snp1,data=sub)
    model2 <- rlm(ldl_res~snp2,data=sub)
    model3 <- rlm(ldl_res~snp1+snp2,data=sub)
    model4 <- rlm(ldl_res~snp1+snp2+snp1:snp2,data=sub)
    ldl_results[n,1] <- snps[i]
    ldl_results[n,2] <- snps[j]
    ldl_results[n,3] <- BIC(model1)
    ldl_results[n,4] <- BIC(model2)
    ldl_results[n,5] <- BIC(model3)
    ldl_results[n,6] <- BIC(model4)
    ldl_results[n,7] <- summary(model4)$coeff[2,1]
    ldl_results[n,8] <- summary(model4)$coeff[3,1]
    ldl_results[n,9] <- summary(model4)$coeff[4,1]
    ldl_results[n,10] <- summary(model4)$coeff[2,3]
    ldl_results[n,11] <- summary(model4)$coeff[3,3]
    ldl_results[n,12] <- summary(model4)$coeff[4,3]
  }
  print(i)
}
rm(i,j,n,sub,model1,model2,model3,model4)
colnames(ldl_results) <- c("SNP1","SNP2","model1_BIC","model2_BIC","model3_BIC","model4_BIC","SNP1_E","SNP2_E","SNP1SNP2_E","SNP1_t","SNP2_t","SNP1SNP2_t")

ldl_results$SNP1_P <- pnorm(abs(ldl_results$SNP1_t),lower.tail=F)*2
ldl_results$SNP2_P <- pnorm(abs(ldl_results$SNP2_t),lower.tail=F)*2
ldl_results$SNP1SNP2_P <- pnorm(abs(ldl_results$SNP1SNP2_t),lower.tail=F)*2

ldl_results_long <- as.data.frame(matrix(NA,378*3,4))
colnames(ldl_results_long) <- c("SNP1","SNP2","name","P")

n <- 1
for(i in 1:378){
  ldl_results_long[n:(n+2),1] <- ldl_results[i,1]
  ldl_results_long[n:(n+2),2] <- ldl_results[i,2]
  ldl_results_long[n:(n+2),3] <- c("SNP1","SNP2","SNP1SNP2")
  ldl_results_long[n:(n+2),4] <- as.numeric(ldl_results[i,13:15])
  n <- n+3
}

# total cholesterol
tc_results <- as.data.frame(matrix(NA,378,12))
n <- 0
for(i in 1:27){
  for(j in (i+1):28){
    n <- n+1
    sub <- lipids_pheno[,c(snps[i],snps[j],"tc_res")]
    colnames(sub) <- c("snp1","snp2","tc_res")
    model1 <- rlm(tc_res~snp1,data=sub)
    model2 <- rlm(tc_res~snp2,data=sub)
    model3 <- rlm(tc_res~snp1+snp2,data=sub)
    model4 <- rlm(tc_res~snp1+snp2+snp1:snp2,data=sub)
    tc_results[n,1] <- snps[i]
    tc_results[n,2] <- snps[j]
    tc_results[n,3] <- BIC(model1)
    tc_results[n,4] <- BIC(model2)
    tc_results[n,5] <- BIC(model3)
    tc_results[n,6] <- BIC(model4)
    tc_results[n,7] <- summary(model4)$coeff[2,1]
    tc_results[n,8] <- summary(model4)$coeff[3,1]
    tc_results[n,9] <- summary(model4)$coeff[4,1]
    tc_results[n,10] <- summary(model4)$coeff[2,3]
    tc_results[n,11] <- summary(model4)$coeff[3,3]
    tc_results[n,12] <- summary(model4)$coeff[4,3]
  }
  print(i)
}
rm(i,j,n,sub,model1,model2,model3,model4)
colnames(tc_results) <- c("SNP1","SNP2","model1_BIC","model2_BIC","model3_BIC","model4_BIC","SNP1_E","SNP2_E","SNP1SNP2_E","SNP1_t","SNP2_t","SNP1SNP2_t")

tc_results$SNP1_P <- pnorm(abs(tc_results$SNP1_t),lower.tail=F)*2
tc_results$SNP2_P <- pnorm(abs(tc_results$SNP2_t),lower.tail=F)*2
tc_results$SNP1SNP2_P <- pnorm(abs(tc_results$SNP1SNP2_t),lower.tail=F)*2

tc_results_long <- as.data.frame(matrix(NA,378*3,4))
colnames(tc_results_long) <- c("SNP1","SNP2","name","P")

n <- 1
for(i in 1:378){
  tc_results_long[n:(n+2),1] <- tc_results[i,1]
  tc_results_long[n:(n+2),2] <- tc_results[i,2]
  tc_results_long[n:(n+2),3] <- c("SNP1","SNP2","SNP1SNP2")
  tc_results_long[n:(n+2),4] <- as.numeric(tc_results[i,13:15])
  n <- n+3
}

# triglycerides

tg_results <- as.data.frame(matrix(NA,378,12))
n <- 0
for(i in 1:27){
  for(j in (i+1):28){
    n <- n+1
    sub <- lipids_pheno[,c(snps[i],snps[j],"tg_res")]
    colnames(sub) <- c("snp1","snp2","tg_res")
    model1 <- rlm(tg_res~snp1,data=sub)
    model2 <- rlm(tg_res~snp2,data=sub)
    model3 <- rlm(tg_res~snp1+snp2,data=sub)
    model4 <- rlm(tg_res~snp1+snp2+snp1:snp2,data=sub)
    tg_results[n,1] <- snps[i]
    tg_results[n,2] <- snps[j]
    tg_results[n,3] <- BIC(model1)
    tg_results[n,4] <- BIC(model2)
    tg_results[n,5] <- BIC(model3)
    tg_results[n,6] <- BIC(model4)
    tg_results[n,7] <- summary(model4)$coeff[2,1]
    tg_results[n,8] <- summary(model4)$coeff[3,1]
    tg_results[n,9] <- summary(model4)$coeff[4,1]
    tg_results[n,10] <- summary(model4)$coeff[2,3]
    tg_results[n,11] <- summary(model4)$coeff[3,3]
    tg_results[n,12] <- summary(model4)$coeff[4,3]
  }
  print(i)
}
rm(i,j,n,sub,model1,model2,model3,model4)
colnames(tg_results) <- c("SNP1","SNP2","model1_BIC","model2_BIC","model3_BIC","model4_BIC","SNP1_E","SNP2_E","SNP1SNP2_E","SNP1_t","SNP2_t","SNP1SNP2_t")

tg_results$SNP1_P <- pnorm(abs(tg_results$SNP1_t),lower.tail=F)*2
tg_results$SNP2_P <- pnorm(abs(tg_results$SNP2_t),lower.tail=F)*2
tg_results$SNP1SNP2_P <- pnorm(abs(tg_results$SNP1SNP2_t),lower.tail=F)*2

tg_results_long <- as.data.frame(matrix(NA,378*3,4))
colnames(tg_results_long) <- c("SNP1","SNP2","name","P")

n <- 1
for(i in 1:378){
  tg_results_long[n:(n+2),1] <- tg_results[i,1]
  tg_results_long[n:(n+2),2] <- tg_results[i,2]
  tg_results_long[n:(n+2),3] <- c("SNP1","SNP2","SNP1SNP2")
  tg_results_long[n:(n+2),4] <- as.numeric(tg_results[i,13:15])
  n <- n+3
}

# Test for SNP-SNP interaction through logistic model fitting and extract BIC as well as interaction model coefficients (beta and t-value) 
# Coronary artery diseases (CAD)
cad_eid <- fread("/camhpc/home/yhuang5/lipids_heiko/ukb_cad_id_list")
lipids_pheno$cad <- ifelse(lipids_pheno$eid %in% cad_eid$V1,1,0)

cad_results <- as.data.frame(matrix(NA,378,12))
n <- 0
for(i in 1:27){
  for(j in (i+1):28){
    n <- n+1
    sub <- lipids_pheno[,c(snps[i],snps[j],"cad","age","sex","smoking","alc_drink","BMI","chl_med","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")]
    colnames(sub) <- c("snp1","snp2","cad","age","sex","smoking","alc_drink","BMI","chl_med","PC1","PC2","PC3","PC4","PC5","PC6","PC7","PC8","PC9","PC10")
    model1 <- glm(cad~snp1+age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=sub,family=binomial(link='logit'))
    model2 <- glm(cad~snp2+age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=sub,family=binomial(link='logit'))
    model3 <- glm(cad~snp1+snp2+age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=sub,family=binomial(link='logit'))
    model4 <- glm(cad~snp1+snp2+snp1:snp2+age+sex+factor(smoking)+factor(alc_drink)+BMI+chl_med+PC1+PC2+PC3+PC4+PC5+PC6+PC7+PC8+PC9+PC10,data=sub,family=binomial(link='logit'))
    cad_results[n,1] <- snps[i]
    cad_results[n,2] <- snps[j]
    cad_results[n,3] <- BIC(model1)
    cad_results[n,4] <- BIC(model2)
    cad_results[n,5] <- BIC(model3)
    cad_results[n,6] <- BIC(model4)
    cad_results[n,7] <- summary(model4)$coeff["snp1",1]
    cad_results[n,8] <- summary(model4)$coeff["snp2",1]
    cad_results[n,9] <- summary(model4)$coeff["snp1:snp2",1]
    cad_results[n,10] <- summary(model4)$coeff["snp1",4]
    cad_results[n,11] <- summary(model4)$coeff["snp2",4]
    cad_results[n,12] <- summary(model4)$coeff["snp1:snp2",4]
  }
  print(i)
}

colnames(cad_results) <- c("SNP1","SNP2","BIC_model1","BIC_model2","BIC_model3","BIC_model4",
                           "SNP1_E","SNP2_E","SNP1SNP2_E","SNP1_P","SNP2_P","SNP1SNP2_P")

cad_results_long <- as.data.frame(matrix(NA,378*3,4))
colnames(cad_results_long) <- c("SNP1","SNP2","name","P")

n <- 1
for(i in 1:378){
  cad_results_long[n:(n+2),1] <- cad_results[i,1]
  cad_results_long[n:(n+2),2] <- cad_results[i,2]
  cad_results_long[n:(n+2),3] <- c("SNP1","SNP2","SNP1SNP2")
  cad_results_long[n:(n+2),4] <- as.numeric(cad_results[i,10:12])
  n <- n+3
}

# Merge all traits (lipids + CAD) and do FDR correction on p-values

hdl_results_long$trait <- "HDL"
ldl_results_long$trait <- "LDL"
tc_results_long$trait <- "TC"
tg_results_long$trait <- "TG"
cad_results_long$trait <- "CAD"

all_results_long <- rbind(hdl_results_long,ldl_results_long,tc_results_long,tg_results_long,cad_results_long)
all_results_long$fdr.q <- p.adjust(all_results_long$P,"fdr")

all_results <- as.data.frame(matrix(NA,(nrow(all_results_long)/3),9))
colnames(all_results) <- c("trait","SNP1","SNP2","SNP1_P","SNP1_fdr_q","SNP2_P","SNP2_fdr_q","SNP1SNP2_P","SNP1SNP2_fdr_q")

for(i in 1:nrow(all_results)){
  all_results[i,1] <- all_results_long[(3*i-2),5]
  all_results[i,2] <- all_results_long[(3*i-2),1]
  all_results[i,3] <- all_results_long[(3*i-2),2]
  all_results[i,4] <- all_results_long[(3*i-2),4]
  all_results[i,5] <- all_results_long[(3*i-2),6]
  all_results[i,6] <- all_results_long[(3*i-1),4]
  all_results[i,7] <- all_results_long[(3*i-1),6]
  all_results[i,8] <- all_results_long[(3*i),4]
  all_results[i,9] <- all_results_long[(3*i),6]
  if(ceiling(i/100)==(i/100)){print(i)}
}

# merge with BIC results

hdl_results$trait <- "HDL"
ldl_results$trait <- "LDL"
tc_results$trait <- "TC"
tg_results$trait <- "TG"
cad_results$trait <- "CAD"
temp <- rbind(select(hdl_results,trait,SNP1,SNP2,model1_BIC,model2_BIC,model3_BIC,model4_BIC,SNP1_E,SNP2_E,SNP1SNP2_E),
              select(ldl_results,trait,SNP1,SNP2,model1_BIC,model2_BIC,model3_BIC,model4_BIC,SNP1_E,SNP2_E,SNP1SNP2_E),
              select(tc_results,trait,SNP1,SNP2,model1_BIC,model2_BIC,model3_BIC,model4_BIC,SNP1_E,SNP2_E,SNP1SNP2_E),
              select(tg_results,trait,SNP1,SNP2,model1_BIC,model2_BIC,model3_BIC,model4_BIC,SNP1_E,SNP2_E,SNP1SNP2_E),
              select(cad_results,trait,SNP1,SNP2,model1_BIC=BIC_model1,model2_BIC=BIC_model2,model3_BIC=BIC_model3,model4_BIC=BIC_model4,SNP1_E,SNP2_E,SNP1SNP2_E))

all_results_merge <- merge(all_results,temp,by=c("trait","SNP1","SNP2"))           
