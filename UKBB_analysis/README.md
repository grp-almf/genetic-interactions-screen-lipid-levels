# UK Biobank (UKBB) analysis of genetic interactions for lipids

**Package prerequisites:**

data.table (>=1.14.0)
dplyr (>=1.0.5)
MASS (>=7.3-51.5)


**Six scripts contain R (3.6.3) code for:**

1. Gene-based PTV-burden association analysis for HDLc, LDLc, total cholesterol (TC) and triglycerides (TG)
2. Gene-based PTV-PTV burden interaction analysis for HDLc, LDLc, total cholesterol (TC) and triglycerides (TG)
3. GWAS loci-based SNP-SNP interaction analysis for HDLc, LDLc, total cholesterol (TC), triglycerides (TG) and CAD
4. Gene-based PTV-burden and GWAS loci-based SNP interaction analysis for HDLc, LDLc, total cholesterol (TC) and triglycerides (TG)
5. Gene-based PTV-burden and GWAS polygenic risk score (PRS) interaction analysis for HDLc, LDLc, total cholesterol (TC) and triglycerides (TG)
6. PTV-burden analysis for LPL stratified on GoF S447Ter

All analyses were run using robust linear regression models (rlm function in MASS), and p-values were calculated based on normal approximation.

FDR-adjustment was done separately for each interaction analysis and FDR-q < 0.005 was chosen as significant threshold for genetic interaction detection in combination with BIC statistics.

Individual level data from the UKBB is available under appropriate application.
All summary level data is available in the supplementary tables.
